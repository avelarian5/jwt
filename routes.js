/**
 * Imports
 *
 * express - https://www.npmjs.com/package/express
 * Fast, unopinionated, minimalist web framework for node.
 */
const express = require("express");

/**
 * Internal imports
 *
 * user - User controller to serve the routes.
 */
const user = require("./controller/user");
const utils = require("./utils");

/**
 * routes - This object will map the routes/endpoints that
 * the server (app) will listen
 */
const routes = express.Router();

/** Routes/Endpoints */
routes.get("/", (_request, response) => {
  response.status(200).json({ status: "Service available." });
});

routes.post("/verify-token", utils.authenticateToken, (_request, response) => {
  return response.status(200).json({ message: "Token is valid." });
});

routes.post("/login", user.login);
routes.post("/register", user.register);
routes.post("/get-new-password", user.getNewPassword);
routes.post(
  "/update-password/:id",
  utils.authenticateToken,
  user.updatePassword
);

module.exports = routes;
