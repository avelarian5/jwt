/**
 * Imports
 *
 * bcrypt - https://www.npmjs.com/package/bcrypt
 * A library to help you hash passwords.
 *
 * JWT - https://www.npmjs.com/package/jsonwebtoken
 * An implementation of JSON Web Tokens.
 */
const fs = require("fs");
const path = require("path");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

/** BCRYPT */
const { SALT_ROUNDS } = require("../constants");

/**
 * hashPassword - Hashs a password passed as an argument
 *
 * @param saltRounds The number of salts of the hash
 * @param password The password that will be hashed
 */
async function hashPassword(password) {
  return bcrypt
    .genSalt(SALT_ROUNDS)
    .then((salt) => {
      return bcrypt.hash(password, salt);
    })
    .catch((err) => {
      throw Error(err);
    });
}

/**
 * comparePassword - Compare the password sent with the password hash
 *
 * @param passwordSent: string
 * @param passwordHashed: string
 * @returns boolean - if they are the same or not
 */
async function comparePassword(passwordSent, passwordHashed) {
  return bcrypt.compare(passwordSent, passwordHashed);
}

/** JWT */
/**
 * createToken - Creates a JWT with our secret and the data sent
 * that lasts 6 hours.
 *
 * @param data: any - Any data to compose the JWT code
 * @returns token: string
 */
function createToken(data) {
  const privateKey =
    process.env.PRIVATE_KEY ??
    fs.readFileSync(path.join(__dirname, "../private.key"));
  return jwt.sign(data, privateKey, { expiresIn: "6h" });
}

/**
 * authenticateToken - This is a middleware that authenticates
 * the token sent to verify if the user has a valid session.
 *
 * @returns It has no return since it will just verify the token
 */
function authenticateToken(request, response, next) {
  const authorizationHeader = request.headers["authorization"];
  const token = authorizationHeader && authorizationHeader.split(" ")[1];

  if (!token)
    return response
      .status(401)
      .json({ error: "You don't have access to this content." });

  const privateKey =
    process.env.PRIVATE_KEY ??
    fs.readFileSync(path.join(__dirname, "../private.key"));
  jwt.verify(token, privateKey, (error, user) => {
    if (error)
      return response
        .status(403)
        .json({ error: "You don't have access to this content." });

    const { id } = request.params;
    if (id && Number(id) !== user.id)
      return response
        .status(403)
        .json({ error: "You don't have access to this content." });

    next();
  });
}

/**
 * sets of charachters
 */
const upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const lower = 'abcdefghijklmnopqrstuvwxyz'
const digit = '0123456789'
const all = upper + lower + digit

/**
 * generate random integer not greater than `max`
 */

function rand (max) {
  return Math.floor(Math.random() * max)
}

/**
 * generate random character of the given `set`
 */

function random (set) {
  return set[rand(set.length - 1)]
}

/**
 * generate an array with the given `length` 
 * of characters of the given `set`
 */

function generate (length, set) {
  var result = []
  while (length--) result.push(random(set))
  return result
}

/**
 * shuffle an array randomly
 */
function shuffle (arr) {
  var result = []

  while (arr.length) {
    result = result.concat(arr.splice(rand[arr.length - 1]))
  }

  return result
}
/**
 * do the job
 */
function password (length) {
  var result = [] // we need to ensure we have some characters

  result = result.concat(generate(1, upper)) // 1 upper case
  result = result.concat(generate(1, lower)) // 1 lower case
  result = result.concat(generate(1, digit)) // 1 digit
  result = result.concat(generate(length - 3, all)) // remaining - whatever

  return shuffle(result).join('') // shuffle and make a string
}

/** Exports */
module.exports = {
  hashPassword,
  comparePassword,
  createToken,
  authenticateToken,
  password,
};
