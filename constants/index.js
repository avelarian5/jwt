/**
 * Regex constants
 *
 * These are the regex constants to verify 
 * if an email or a password has the correct format.
 */
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

/**
 * Bcrypt constants
 *
 * These are the bcrypt constants that we choose for our cryptography
 */
const SALT_ROUNDS = 10;

module.exports = { EMAIL_REGEX, PASSWORD_REGEX, SALT_ROUNDS };
