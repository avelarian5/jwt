/**
 * Imports
 */
const utils = require("../../utils");

/**
 * seed - This file contains the seed function, it will be executed to populate the database.
 * @param knex This argument is the instance of the connection to the database.
 * @returns This function returns a `Promise` that will insert the seed to the database.
 */
exports.seed = async function (knex) {
  await utils.hashPassword("Simplon123").then((hash) => {
    // Deletes ALL existing entries
    return knex("users")
      .del()
      .then(function () {
        // Inserts seed entries
        return knex("users").insert([
          {
            updated_at: new Date(),
            created_at: new Date(),
            email: "ian.avelar.simplon@gmail.com",
            password: hash,
          },
        ]);
      });
  });
};
