/** Users table migration
 * This file contains the migration of the `users` table, \
 * where information related to all users registered in the system is being stored.
 *
 * @param knex This argument is the instance of the connection to the database.
 * @returns This function returns a `Promise` that will make changes to the database.
 */

exports.up = function (knex) {
  return knex.schema.createTable("users", function (table) {
    table.increments("id").unique().primary().notNullable();
    table.timestamp("archived_at", { useTz: true });
    table.timestamp("updated_at", { useTz: true }).notNullable();
    table.timestamp("created_at", { useTz: true }).notNullable();
    table.string("email", 255).notNullable();
    table.string("password", 255).notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("users");
};

exports.config = { transaction: false };
