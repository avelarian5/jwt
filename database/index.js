/**
 * Imports
 *
 * knex library - http://knexjs.org/
 * Knex is a SQL Query Builder, that is, it builds the queries for us,
 * so we must not directly write SQL commands.
 *
 * path library
 * This bookstore helps us deal with file paths
 */
const knex = require("knex");
const path = require("path");

/**
 * Constants
 *
 * knexConfig
 * This object represents the settings selected for the knex,
 * it will follow the rules defined within this object,
 * such as the connection credentials to the database,
 * details about the migrations, etc.
 *
 * database
 * This object is the instance of the connection to the database,
 * through which it is possible to carry out the migrations,
 * modify the data, etc.
 */
const knexConfig = {
  client: "sqlite3",
  connection: {
    filename: path.join(__dirname, "./dev.sqlite3"),
  },
  useNullAsDefault: true,
  seeds: {
    directory: path.join(__dirname, "./seeds"),
  },
  migrations: {
    tableName: "simplon_migrations",
    directory: path.join(__dirname, "./migrations"),
  },
};
const database = knex(knexConfig);

/**
 * Database related functions
 *
 * These functions directly use the instance of the database connection,
 * so they are structural functions of the database,
 * so they are located in that file.
 */

/**
 * migrateDatabase - Runs the lastest migrations.
 * @returns a `Promise` that will run the lastest migrations
 */
async function migrateDatabase() {
  return await database.migrate.latest();
}

/**
 * seedDatabase - Insert the seeds to the database.
 * We call 'seed` the files or files that contain false data for testing purposes.
 * @returns a `Promise` that will insert the seeds
 */
async function seedDatabase() {
  return await database.seed.run(path.join(__dirname, "./seeds"));
}

/**
 * createSeed - Create a seed file.
 * We call 'seed` the files or files that contain false data for testing purposes.
 * @returns a `Promise` that will create the seed file.
 */
async function createSeed(seedName) {
  return await database.seed.make(seedName, {
    extension: "js",
  });
}

/**
 * rollbackDatabase - Rollbacks all the database migrations.
 * @returns a `Promise` that will rollback all the database migrations.
 */
async function rollbackDatabase() {
  return await database.migrate.rollback(undefined, true);
}

/**
 * rollbackMigration - Rollbacks the last database migration.
 * @returns a `Promise` that will rollback the last database migration.
 */
async function rollbackMigration() {
  return await database.migrate.rollback(undefined, false);
}

/**
 * destroyDatabaseConnection - Destroys the instance of the database connection.
 * @returns a `Promise` that will destroy the instance of the database connection.
 */
async function destroyDatabaseConnection() {
  return await database.destroy();
}

/**
 * Exports
 */
module.exports = {
  table: database,
  migrateDatabase,
  createSeed,
  seedDatabase,
  rollbackMigration,
  rollbackDatabase,
  destroyDatabaseConnection,
};
