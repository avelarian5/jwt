/**
 * Imports
 *
 * database - It is the instance of the connection to the database.
 */
const database = require("../database");

/**
 * Constants
 *
 * help - Contains the help message of the db:make-seed command
 * argv - Contains the arguments inserted at the terminal
 */
const help = `Usage: npm run db:make-migration <migration-name>`;
const { argv } = process;

/**
 * This conditions makes sure that we are receiving a name for the seed.
 */
if (argv.length !== 3)
  throw new Error(
    `Unexpected number of arguments: ${JSON.stringify(argv)}\n${help}`
  );
const seedName = argv[2];
if (!seedName) throw new Error(`Unexpected missing seed name: \n${help}`);

/**
 * This is where the magic happens!
 *
 * Using the `database` object (instance of the connection to the database).
 * First, we access the key `seed`, the same one has a function `make`
 * where we pass the name of the seed to be created and
 * what is the extension that we are working.
 * We use a `then` function to execute something after the execution
 * of the previous command, so we show in the terminal
 * that the command was carried out successfully.
 * If there is an error, the `catch` function will be executed.
 */
database
  .createSeed(seedName.trim())
  .then(() => {
    console.log("Seed file created");
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
