/**
 * Imports
 *
 * database - It is the instance of the connection to the database.
 */
const database = require("../database");

/**
 * This is where the magic happens!
 *
 * Using the `database` object (instance of connection to the database),
 * we call the function `rollbackDatabase` which will rollback all migrations
 * to the database. Afterwards, we will destroy
 * the connection with the database because it is possible
 * that some imports do not use the same object and thus we
 * create several instances of connection with the database and
 * this is not advantageous. Therefore, we show that the process
 * was well executed with the `then` function and we watch for
 * errors with the` catch` function.
 */
database
  .rollbackDatabase()
  .then(database.destroyDatabaseConnection)
  .then(() => console.log("Rollback done"))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
