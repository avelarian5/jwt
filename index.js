/**
 * Imports
 *
 * express - https://www.npmjs.com/package/express
 * Fast, unopinionated, minimalist web framework for node.
 *
 * bodyParser - https://www.npmjs.com/package/body-parser
 * Node.js body parsing middleware.
 * 
 * cors
 * 
 */
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

/**
 * Internal imports
 * 
 * routes - This file will keep all the route organization
 */
const routes = require("./routes");

/**
 * Constants
 *
 * app - Our server instance.
 * port - The port that we application will be hosted.
 */
const app = express();
const port = 3333;

/** Middlewares */
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());

/** Routes */
app.use(routes);

/** Port related code */
app.listen(process.env.PORT ?? port, () => {
  console.log(`App is running on port ${port}`);
});
