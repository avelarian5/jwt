# Backend

**`Backend` is a REST Node.js API.**

## ⚡️ Requirements

- A solid environement with [Node.js](https://nodejs.org/en/) (`v14.15` or newer)

## 🔥 Stack

- [REST](https://pt.wikipedia.org/wiki/REST), Representational State Transfer

## 💻 How to use

You're going to want to install the repository:

```shell
$ npm
```

Now you need to prepare the database:

- Firstly, you need to reset the database;

```shell
$ npm run db:rollback
```

- So, run the migrations to prepare the database:

```shell
$ npm run db:migrate
```

- If you want, you can insert the seeds to populate the database:

```shell
$ npm run db:seed
```

Now, you are ready to go! 🚀

## 🕺 Contribute

**Want to hack our system? Follow the next instructions: 🚀**

1. Fork this repository to your own GitHub account and then clone it to your local device
2. Install dependencies on each project
3. Ensure that the tests are passing
4. Send a pull request 🙌

Remember to add tests for your change if possible.