/** Imports */
const nodemailer = require("nodemailer");

const model = require("../model/user");
const utils = require("../utils");
const { EMAIL_REGEX, PASSWORD_REGEX } = require("../constants");

/**
 * Exports
 * 
 * We are exporting the group of functions 
 * that make up the user controller.
 */
module.exports = {
  /**
   * register - Register a new user.
   *
   * @returns { message: string, user: { id: number, email: string } }
   */
  register: async (request, response) => {
    /**
     * We need to receive an email and a password, thoses are the minimum
     * information that we need to create a user.
     */
    const { email, password } = request.body;

    /**
     * We need to verify if:
     * - The email and the password are not null or undefined;
     * - The email has the correct format (email@company.com);
     * - The password has at least 8 characters, if it includes a number;
     * a capital letter and a low case letter;
     * - The email is unique.
     */
    if (!email || !password) {
      return response
        .status(406)
        .json({ error: "All the fields are required for registering." });
    }

    if (!EMAIL_REGEX.test(email)) {
      return response
        .status(400)
        .json({ error: "The email format is not valid." });
    }

    if (!PASSWORD_REGEX.test(password)) {
      return response.status(400).json({
        error:
          "The password must have at least 8 characters, include a number, a capital letter and a lower case letter.",
      });
    }

    const emails = await model.findByEmail(email);
    if (emails.length > 0) {
      return response
        .status(406)
        .json({ error: "This email is already on use." });
    }

    /**
     * If everything is correct,
     * Before the insertion of the user to the database
     * we need to hash his password because if anyone stole
     * our informations, this hacker will not have access to
     * the passwords.
     */
    const hashedPassword = await utils.hashPassword(password);

    /**
     * Finally, we make the creation and return just the
     * essential information to the client.
     */
    const id = await model.create(email, hashedPassword);
    const { id: userId, email: userEmail } = await model.findById(id[0]);
    return response.status(201).json({
      message: "The user has been created.",
      user: { id: userId, email: userEmail },
    });
  },
  /**
   * login - Initiates a user session.
   *
   * @returns { id: number, email: string  }
   */
  login: async (request, response) => {
    /**
     * We need to receive an email and a password, thoses are the minimum
     * information that we need to login a user.
     */
    const { email, password } = request.body;

    /**
     * We need to verify if:
     * - The email and the password are not null or undefined;
     * - The email has the correct format (email@company.com);
     * - The password has at least 8 characters, if it includes a number;
     * a capital letter and a low case letter;
     * (Obs.: Because if they're not, it means that the password was not
     * registered on our system, so we can directly reject the login.)
     * - The email is unique.
     */
    if (!email || !password) {
      return response
        .status(406)
        .json({ error: "All the fields are required for registering." });
    }

    if (!EMAIL_REGEX.test(email)) {
      return response
        .status(400)
        .json({ error: "The credentials sent are not correct." });
    }

    if (!PASSWORD_REGEX.test(password)) {
      return response.status(400).json({
        error: "The credentials sent are not correct.",
      });
    }

    const users = await model.findByEmail(email);
    if (users.length === 0) {
      return response.status(404).json({ error: "User not found." });
    }
    if (users.length > 1) {
      return response.status(500).json({ error: "More than one user found." });
    }

    /**
     * If everything is correct,
     * We need to compare the password sent with the one
     * inside the database, this function compare the un-hashed
     * password sent with the hashed one.
     */
    const user = users[0];
    const passwordIsValid = await utils.comparePassword(
      password,
      user.password
    );

    if (!passwordIsValid) {
      return response
        .status(403)
        .json({ error: "The credentials sent are not correct." });
    }

    /**
     * If the password match we can assign a JWT token
     * to the user. It means that he has a key of access to our
     * system informations.
     */
    const token = utils.createToken(user);

    return response.status(200).json({
      message: "Session initialized.",
      user: { id: user.id, email: user.email },
      token,
    });
  },
  updatePassword: async (request, response) => {
    /**
     * We need to receive an email and a password, thoses are the minimum
     * information that we need to create a user.
     */
    const { id } = request.params;
    const { oldPassword, newPassword } = request.body;

    /**
     * We need to verify if:
     * - The old password matches;
     * - The new password has at least 8 characters, if it includes a number;
     * a capital letter and a low case letter;
     */
    if (!oldPassword || !newPassword) {
      return response
        .status(406)
        .json({ error: "All the fields are required for updating password." });
    }
    if (!PASSWORD_REGEX.test(oldPassword)) {
      return response
        .status(400)
        .json({ error: "The old password is not valid." });
    }
    if (!PASSWORD_REGEX.test(newPassword)) {
      return response.status(400).json({
        error:
          "The new password must have at least 8 characters, include a number, a capital letter and a lower case letter.",
      });
    }

    /**
     * If everything is correct,
     * We need to compare the old password sent with the one
     * inside the database, this function compare the un-hashed
     * password sent with the hashed one.
     */
    const user = await model.findById(id);
    const passwordIsValid = await utils.comparePassword(
      oldPassword,
      user.password
    );

    if (!passwordIsValid) {
      return response
        .status(403)
        .json({ error: "The old password is not valid." });
    }

    /**
     * Finally, we make the update and return just the
     * essential information to the client.
     */
    await model.update(id, {
      password: await utils.hashPassword(newPassword),
    });
    return response.status(201).json({
      message: "The user's password has been updated.",
    });
  },
  getNewPassword: async (request, response) => {
    /**
     * We need to receive an email, this is the minimum
     * information that we need to get a new user password.
     */
    const { email } = request.body;

    /**
     * We need to verify if:
     * - The email is valid;
     */
    if (!email) {
      return response
        .status(406)
        .json({ error: "All the fields are required for updating password." });
    }
    if (!EMAIL_REGEX.test(email)) {
      return response
        .status(400)
        .json({ error: "The credentials sent are not correct." });
    }
    const users = await model.findByEmail(email);
    if (users.length === 0) {
      return response.status(404).json({ error: "User not found." });
    }
    if (users.length > 1) {
      return response.status(500).json({ error: "More than one user found." });
    }

    /**
     * If everything is correct,
     */

    /** Nodemailer */
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "nodemailer.simplon@gmail.com",
        pass: "Simplon123",
      },
    });
    
    const newPassword = utils.password(8);
    const mailOptions = {
      from: "nodemailer.simplon@gmail.com",
      to: email,
      subject: "New password",
      text: newPassword,
    };

    await transporter
      .sendMail(mailOptions)
      .then(async (info) => {
        console.log(info);
        const user = users[0];
        await model.update(user.id, {
          password: await utils.hashPassword(newPassword),
        });
        return response
          .status(200)
          .json({ message: "A new password has been sent to your email." });
      })
      .catch((error) => {
        console.log(error);
        return response
          .status(500)
          .json({ error: "An unexpected error happened." });
      });
  },
};