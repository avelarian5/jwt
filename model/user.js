/**
 * Imports
 * 
 * database - It is the instance of the connection to the database.
 */
const database = require("../database");

/**
 * Exports
 * 
 * We are exporting the group of functions 
 * that make up the user model.
 */
module.exports = {
  /**
   * findById - Find a user by his id.
   *
   * @param id: string - The id of the user to be selected.
   * @returns { id: string, email: string, password: string, created_at: number, updated_at: number, archived_at: number || null}
   */
  findById: (id) => {
    return database.table("users").where("id", id).select("*").first();
  },
  /**
   * findByEmail - Find a user by his email.
   *
   * @param email: string - The email of the user to be selected.
   * @returns { id: string, email: string, password: string, created_at: number, updated_at: number, archived_at: number || null}
   */
  findByEmail: (email) => {
    return database.table("users").where("email", email).select("*");
  },
  /**
   * create - Creates a new user.
   *
   * @param email: string - The user's email to be inserted.
   * @param password: string - The user's password to be inserted.
   * @returns string[]
   */
  create: (email, password) => {
    return database.table("users").insert({
      email,
      password,
      created_at: new Date(),
      updated_at: new Date(),
    });
  },
  /**
   * update - Updates a user.
   * 
   * @param id: string - The user's id.
   * @param user: any - The user's information that will be updated.
   * @returns string[]
   */
  update: (id, user) => {
    return database.table("users").update(user).where("id", id);
  },
};
